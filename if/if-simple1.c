// Check Even or Odd
// https://www.programiz.com/c-programming/examples/even-odd

#include <stdio.h>
int main()
{
    int number;
    printf("Enter an integer: ");
    scanf("%d", &number);
    // True if the number is perfectly divisible by 2
    if(number % 2 == 0)
        printf("%d is even.", number);
    else
        printf("%d is odd.", number);
    return 0;
}

// OUTPUT
// D:\>gcc -o if-simple1.exe if-simple1.c
// D:\>if-simple1
// Enter an integer: 13
// 13 is odd.
// D:\>if-simple1
// Enter an integer: 22
// 22 is even.
