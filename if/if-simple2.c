// Check Odd or Even Using Conditional Operator
// https://www.programiz.com/c-programming/examples/even-odd

#include <stdio.h>
int main()
{
    int number;
    printf("Enter an integer: ");
    scanf("%d", &number);
    (number % 2 == 0) ? printf("%d is even.", number) : printf("%d is odd.", number);
    return 0;
}


// OUTPUT
// D:\>gcc -o if-simple2.exe if-simple2.c
// D:\>if-simple2
// Enter an integer: 19
// 19 is odd.
// D:\>if-simple2
// Enter an integer: 88
// 88 is even.
